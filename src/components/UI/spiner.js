import React from 'react';

const spinner = () => (
    <div className="lds-double-ring flex flex-center">
        <div></div>
        <div></div>
    </div>
);

export default spinner;
