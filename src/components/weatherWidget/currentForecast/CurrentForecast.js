import React from 'react';
import { convertKelwinToCelsius, convertMillisecondsToCurrentTime, convertSecToCurrentTime } from '../../../utility';
import { translateConfig } from '../../../config';

const currentFrecast = ({ main, wind, weather, time }) => {
    const convertedTime = (typeof time === 'string')
        ? convertMillisecondsToCurrentTime(time)
        : convertSecToCurrentTime(time);
    return (
        <div className="main-forecast flex flex-column">
            <img
                className="main-forecast__img"
                src={`//openweathermap.org/img/wn/${weather[0].icon}@2x.png`} alt={weather[0].main}
            />
            <div className="main-forecast__details">
                <div className="main-forecast__details-item flex justify-between">
                    <span>{translateConfig.time}:</span>
                    <span>{convertedTime}</span>
                </div>
                <div className="main-forecast__details-item flex justify-between">
                    <span>{translateConfig.temp}:</span>
                    <span>{convertKelwinToCelsius(main.temp)} &deg;C</span>
                </div>
                <div className="main-forecast__details-item flex justify-between">
                    <span>{translateConfig.humidity}:</span>
                    <span>{main.humidity}%</span>
                </div>
                <div className="main-forecast__details-item flex justify-between">
                    <span>{translateConfig.pressure}:</span>
                    <span>{main.pressure} hpa</span>
                </div>
                <div className="main-forecast__details-item flex justify-between">
                    <span>{translateConfig.windSpeed}:</span>
                    <span>{wind.speed} m/s</span>
                </div>
            </div>

        </div>
    );
}

export default currentFrecast;
