import React from 'react';
import { convertKelwinToCelsius, convertMillisecondsToCurrentTime } from '../../../utility';

const hourlyFrecast = ({ item }) => (
    <div key={item.dt} className="hourly-forecast flex justify-around align-center">
        <div>{convertMillisecondsToCurrentTime(item.dt_txt)}</div>
        <div>{convertKelwinToCelsius(item.main.temp)} &deg;C</div>
        <img
            className="hourly-forecast__img"
            src={`//openweathermap.org/img/wn/${item.weather[0].icon}@2x.png`}
            alt={`${item.weather[0].main}`}
        />
    </div>
);

export default hourlyFrecast;
