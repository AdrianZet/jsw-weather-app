import { connect } from 'react-redux';
import React, { Component } from 'react';

import * as actions from '../../store/actions/index';
import CurrentForecast from './currentForecast/CurrentForecast';
import HourlyForecast from './hourlyForecast/HourlyForecast';
import Spiner from '../UI/spiner';

class WeatherWidget extends Component {
    componentDidMount() {
        const interval = 600000; //fetch data every 10 min
        this.props.onFetchData();
        setInterval(() => this.props.onFetchData(), interval);
    }

    render() {
        let forecastList, forecastLarge, currentIn, forecastAfterFinish;
        if (this.props.weatherData) {
            const { main, wind, weather, dt } = this.props.weatherData.current;
            const { longterm, current } = this.props.weatherData;
            const forecast = {
                dt: current.dt,
                main: {
                    temp: current.main.temp
                },
                weather: [
                    {
                        icon: current.weather[0].icon,
                        main: current.weather[0].main
                    }
                ]
            };

            forecastLarge = <CurrentForecast
                main={longterm[2].main}
                wind={longterm[2].wind}
                time={longterm[2].dt_txt}
                weather={longterm[2].weather} />

            forecastList = longterm.slice(0, 2).map(item => <HourlyForecast key={item.dt} item={item} />);
            currentIn = <HourlyForecast item={forecast} />
            forecastAfterFinish = <HourlyForecast item={longterm[3]} />

            if (this.props.history.location.pathname === '/end') {
                forecastLarge = <CurrentForecast main={main} wind={wind} weather={weather} time={dt} />;
                forecastList = longterm.slice(1, 5).map(item => <HourlyForecast key={item.dt} item={item} />);
            }
        }
        return (
            <div className="app">
                <div className="weather-app flex">
                    {this.props.loading ? <Spiner /> : null}
                    {this.props.history.location.pathname === '/end' ?
                        <>
                            <div className="weather-app__current-wrapper flex-column justify-center align-center">
                                {forecastLarge}
                            </div>
                            <div className="weather-app__hourly-wrapper">
                                {forecastList}
                            </div>
                        </> :
                        <>
                            {currentIn}
                            <div className="weather-app__hourly-wrapper">
                                {forecastList}
                            </div>
                            <div className="weather-app__current-wrapper flex-column justify-center align-center">
                                {forecastLarge}
                            </div>
                            {forecastAfterFinish}
                        </>
                    }
                </div>
            </div >
        );
    }
}

const mapStateToProps = state => {
    return {
        weatherData: state.data.weatherData,
        loading: state.data.loading,
        error: state.data.error
    };
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onFetchData: () => { dispatch(actions.fetchData()) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WeatherWidget);
