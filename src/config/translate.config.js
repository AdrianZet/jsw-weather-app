export const translateConfig = {
    temp: 'temperatura',
    pressure: 'ciśnienie',
    humidity: 'wilgotność',
    windSpeed: 'szybkość wiatru',
    time: 'godzina'
};
