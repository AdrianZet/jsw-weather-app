import * as actionTypes from './actionTypes';
import axios from 'axios';
import { appConfig } from '../../config/index';

export const fetchDataStart = () => {
    return {
        type: actionTypes.FETCH_DATA_START
    };
};

export const fetchDataSuccess = (data) => {
    return {
        type: actionTypes.FETCH_DATA_SUCCESS,
        data: data,
    };
};

export const fetchDataFail = (error) => {
    return {
        type: actionTypes.FETCH_DATA_FAIL,
        error: error
    };
};

export const fetchData = () => {
    return dispatch => {
        dispatch(fetchDataStart());
        let data = {
            current: null,
            longterm: null
        };
        axios.get(appConfig.currentWeather)
            .then(res => {
                data.current = res.data;
                axios.get(appConfig.forecastWeather)
                    .then(res => {
                        data.longterm = res.data.list.slice(0, 7);
                        dispatch(fetchDataSuccess(data));
                    })
                    .catch(err => {
                        dispatch(fetchDataFail(err));
                    });
            })
            .catch(err => {
                dispatch(fetchDataFail(err));
            });
    }
};
