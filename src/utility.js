const moment = require('moment');

export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const convertKelwinToCelsius = (f) => {
    return Math.round((f - 273.15));
};

export const convertMillisecondsToCurrentTime = (timestamp) => {
    return moment(timestamp).format('H:mm');
};

export const convertSecToCurrentTime = (timestamp) => {
    return moment.unix(timestamp).format('H:mm');
};
