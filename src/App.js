import { Route, Switch } from 'react-router-dom';
import React, { Component } from 'react';

import WeatherWidget from './components/weatherWidget/WeatherWidget';

import './styles/styles.scss';

class App extends Component {
    render() {
        return (
            <Switch>
                <Route path="/" exact component={WeatherWidget} />
                <Route path="/end" exact component={WeatherWidget} />
            </Switch>
        );
    }
}

export default App;
